import React from "react";
import MoviesList from "./MoviesList";
import { useSelector } from "react-redux";

function SecoundryContainer() {
  const movies = useSelector((store) => store.movies);
  //console.log(movies);
  return (
    movies.nowPlayingMovies && (
      <div className="bg-black">
        <div className="mt-0 md:-mt-40 pl-4 md:pl-12 relative z-20">
          <MoviesList title={"Now Playing"} movies={movies?.nowPlayingMovies} />
          <MoviesList title={"Top Rated"} movies={movies?.topRatedMovies} />
          <MoviesList title={"Popular"} movies={movies?.popularMovies} />
          <MoviesList title={"Upcoming"} movies={movies?.upcomingMovies} />
        </div>
      </div>
    )
  );
}

export default SecoundryContainer;
