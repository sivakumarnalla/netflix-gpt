import React from "react";
import { IMG_CDN_URL } from "../utils/constants";

const MoviesCard = ({ posterPath }) => {
  //console.log(posterPath);
  // console.log(IMG_CDN_URL + posterPath);
  if (!posterPath) return null;
  return (
    <div className="w-30 md:w-40 pr-2">
      <img alt="movie Card" src={IMG_CDN_URL + posterPath} />
    </div>
  );
};

export default MoviesCard;
