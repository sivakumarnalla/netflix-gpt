export const LOGO =
  "https://cdn.cookielaw.org/logos/dd6b162f-1a32-456a-9cfe-897231c7763c/4345ea78-053c-46d2-b11e-09adaef973dc/Netflix_Logo_PMS.png";

export const API_OPTIONS = {
  method: "GET",
  headers: {
    accept: "application/json",
    Authorization: "Bearer " + process.env.REACT_APP_TBDB_KEY,
  },
};

export const IMG_CDN_URL = "https://image.tmdb.org/t/p/w200";

export const SUPPORTED_LANGUAGES = [
  { identifier: "en", name: "English" },
  { identifier: "hindi", name: "Hindi" },
  { identifier: "telugu", name: "Telugu" },
];

export const OPENAI_KEY = "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";
