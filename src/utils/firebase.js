// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBytaoC8dJ_nY-Ji_qZIXhXxz_0oeGyzmA",
  authDomain: "neflixgpt-f2284.firebaseapp.com",
  projectId: "neflixgpt-f2284",
  storageBucket: "neflixgpt-f2284.appspot.com",
  messagingSenderId: "150815302861",
  appId: "1:150815302861:web:ef3e3986e69dd891773584",
  measurementId: "G-F4EPMXJ2BG",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

export const auth = getAuth();
